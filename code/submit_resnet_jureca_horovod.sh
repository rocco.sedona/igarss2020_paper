#!/bin/bash -x
#SBATCH --account=cstdl
#SBATCH --nodes=32
#SBATCH --ntasks=128
#SBATCH --ntasks-per-node=4
#SBATCH --output=resnet_jureca_16k_w5_1_poly_lr_reduced-out
#SBATCH --error=resnet_jureca_16k_w5_1_poly_lr_reduced-err
#SBATCH --time=3:00:00
#SBATCH --gres=gpu:4 --partition=gpus

module purge
module load GCC/8.3.0  MVAPICH2/2.3.2-GDR

module load TensorFlow/1.13.1-GPU-Python-3.6.8
module load Horovod/0.16.2-GPU-Python-3.6.8
module load Keras/2.2.4-GPU-Python-3.6.8
module load scikit/2019a-Python-3.6.8
module load mpi4py/3.0.1-Python-3.6.8

# Use this only the first time
#python3 -m venv /p/project/joaiml/remote_sensing/rocco_sedona/.env_juwels 
 
# Use this every time for activate the environment
#source /p/project/joaiml/remote_sensing/rocco_sedona/.env_juwels/bin/activate

# Run the program
#four options for the argument to select the setup: "super" for 1 branch super-resolution, "rgb" for 1 branch rgb without pre-training on imagenet
#"rgb_imagenet" for 1 branch rgb with pre-training on imagenet, "t_branches" for 3 branches (each branch operating at a different resolution) CNN
#second argument is the initial epoch, to be set to the desired checkpoint
#srun --cpu_bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_LARS_lr.py -m 'super' -i '0' -e '100' -b '256' -w '5'
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_standard.py -m 'super' -i '0' -e '10' -b '64' -w '5'
#srun --cpu_bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_emergency_poly.py -m 'super' -i '0' -e '10' -b '256' -w '5' -f 'resnet_super_poly-out'

#paper review
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_standard.py -m 'super' -i '0' -e '100' -b '16' -w '5' -s 'ad_conv'
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_standard.py -m 'super' -i '0' -e '100' -b '64' -w '5' -s 'rem_mp'
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_standard.py -m 'super' -i '0' -e '100' -b '64' -w '5' -p '/p/project/ccstdl/remote_sensing/rocco_sedona/Test_upsampled_shuffled.h5'
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_standard.py -m 'super' -i '0' -e '100' -b '64' -w '5'

#igarss paper 
#srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_LARS_resnet18.py -m 'super' -i '0' -e '100' -b '64' -w '5'
srun --cpu-bind=none python /p/project/joaiml/remote_sensing/Data/script/script_horovod_chunks_LARS_new.py -m 'super' -i '0' -e '100' -b '128' -w '5'


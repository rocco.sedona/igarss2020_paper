Code for the IGARSS2020 paper.
In the code folder you can find the Python scripts with the implementation of the models and the bash script to request the resources on the HPC machine (JURECA supercomputer).
A script to run the experiments in a systematic way on HPC systems at JSC using the JUBE module is provided as well.